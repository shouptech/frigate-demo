# Frigate Demo

This is intended to be a simple demo of [Frigate NVR](https://frigate.video/). Frigate NVR is an open source NVR built around real time AI object detection.

This demo also uses an MQTT broker (Eclipse Mosquitto) to show an example of how one might receive event notifications. This can be [ingtegrated with Home Assistant](https://docs.frigate.video/integrations/home-assistant/) for performing automated actions.

## Hardware

Demo hardware:

- [Raspberry Pi 4](raspbian-install.md)
- [Ubiquiti UVC G3 Camera](uvc-g3.md)
- Coral TPU USB AI accelerator

See [Frigate's recommended hardware setup](https://docs.frigate.video/frigate/hardware).

## Frigate Install

The following configurations are how this demo was configured, using the hardware above. 

All data for Frigate are stored in the directory `/opt/frigate`

```shell
sudo mkdir -p /opt/frigate/storage
sudo mkdir -p /opt/frigate/mosquitto
sudo vi /opt/frigate/mosquitto/mosquitto.conf
sudo vi /opt/frigate/config.yml
sudo vi /opt/frigate/docker-compose.yml
```

### `/opt/frigate/mosquitto/mosquitto.conf`

```
listener 1883 0.0.0.0
allow_anonymous true
```

### `/opt/frigate/config.yml`

```yaml
detectors: # <---- add detectors
  coral:
    type: edgetpu
    device: usb

objects:
  track:
    # See: https://docs.frigate.video/configuration/objects
    - person
    - dog
    - cat

mqtt:
  host: mosquitto

cameras:
  demo: # <------ Name the camera
    ffmpeg:
      hwaccel_args: preset-rpi-64-h264
      inputs:
        - path: rtsp://frigate:demo@10.30.14.233:554/s0 # <----- The stream you want to use for detection
          roles:
            - detect
    detect:
      enabled: True
      width: 1280
      height: 720
      fps: 10

record:
  enabled: True
  events:
    retain:
      default: 3

birdseye:
  enabled: True
  mode: continuous
```

### `/opt/frigate/docker-compose.yml`

```yaml
version: "3.9"
services:
  mosquitto:
    container_name: mosquitto
    restart: unless-stopped
    image: eclipse-mosquitto
    ports:
      - "1883:1883"
    volumes:
      - /opt/frigate/mosquitto/mosquitto.conf:/mosquitto/config/mosquitto.conf
  frigate:
    container_name: frigate
    privileged: true # this may not be necessary for all setups
    restart: unless-stopped
    image: ghcr.io/blakeblackshear/frigate:stable
    shm_size: "256mb" # update for your cameras based on calculation above
    devices:
      - /dev/bus/usb:/dev/bus/usb # passes the USB Coral, needs to be modified for other versions
      - /dev/video10:/dev/video10 # passes the video devices for Raspberry Pi hwaccel
      - /dev/video11:/dev/video11
      - /dev/video12:/dev/video12
      - /dev/video13:/dev/video13
      - /dev/video14:/dev/video14
      - /dev/video15:/dev/video15
      - /dev/video16:/dev/video16
      - /dev/video18:/dev/video18
      - /dev/video19:/dev/video19
      - /dev/video20:/dev/video20
      - /dev/video21:/dev/video21
      - /dev/video22:/dev/video22
      - /dev/video23:/dev/video23
      - /dev/video31:/dev/video31

    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /opt/frigate/config.yml:/config/config.yml
      - /opt/frigate/storage:/media/frigate
      - type: tmpfs # Optional: 1GB of memory, reduces SSD/SD Card wear
        target: /tmp/cache
        tmpfs:
          size: 1000000000
    ports:
      - "5000:5000"
      - "8554:8554" # RTSP feeds
      - "8555:8555/tcp" # WebRTC over tcp
      - "8555:8555/udp" # WebRTC over udp
    environment:
      FRIGATE_RTSP_PASSWORD: "password"
```

### Running Frigate

Start frigate with the command:

```shell
cd /opt/frigate
docker-compose up -d
docker-compose logs -f
```

Open your web browser to `http://<ipaddr>:5000`. You should see the Frigate UI.

### MQTT

View MQTT events using `mosquitto_sub`.

MQTT events listed: https://docs.frigate.video/integrations/mqtt/

Can be integrated with Home Assistant.

```
docker run --rm -it eclipse-mosquitto mosquitto_sub -h <ipaddr> -t 'frigate/events'
```

Example event:

```json
{
  "before": {
    "id": "1706378902.029815-t53njd",
    "camera": "demo",
    "frame_time": 1706378902.029815,
    "snapshot_time": 0,
    "label": "person",
    "sub_label": null,
    "top_score": 0,
    "false_positive": true,
    "start_time": 1706378902.029815,
    "end_time": null,
    "score": 0.59765625,
    "box": [
      1133,
      132,
      1279,
      562
    ],
    "area": 62780,
    "ratio": 0.3395348837209302,
    "region": [
      712,
      0,
      1280,
      568
    ],
    "stationary": false,
    "motionless_count": 0,
    "position_changes": 0,
    "current_zones": [],
    "entered_zones": [],
    "has_clip": false,
    "has_snapshot": false
  },
  "after": {
    "id": "1706378902.029815-t53njd",
    "camera": "demo",
    "frame_time": 1706378902.428891,
    "snapshot_time": 1706378902.428891,
    "label": "person",
    "sub_label": null,
    "top_score": 0.70703125,
    "false_positive": false,
    "start_time": 1706378902.029815,
    "end_time": null,
    "score": 0.7265625,
    "box": [
      975,
      138,
      1277,
      719
    ],
    "area": 175462,
    "ratio": 0.5197934595524957,
    "region": [
      204,
      0,
      1280,
      1076
    ],
    "stationary": false,
    "motionless_count": 3,
    "position_changes": 1,
    "current_zones": [],
    "entered_zones": [],
    "has_clip": true,
    "has_snapshot": false
  },
  "type": "new"
}
```

### Setting up motion mask

Motion masks are used to prevent unwanted motion from triggering detection.

See https://docs.frigate.video/configuration/masks/
