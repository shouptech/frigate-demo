# Install of Raspberry Pi OS

## Imaging

Use the Raspberry Pi Imager to image an SD card

1. Chose Raspberry Pi OS (other) -> Raspberry Pi OS Lite (64-bit)
2. Chose internal SD card reader, or whatever storage your SD card appears as
3. Click the gear
    - Enable SSH, use password auth, set public-key auth
    - Set a username and password
    - Optionally, configure the rest. Highly recommend using a wired network though.
4. Write the image!
5. Boot the PI and SSH into it! You may need to look for DHCP leases on your router to find its IP address


## Installing Docker

SSH to PI

```shell
sudo apt update
sudo apt install docker.io docker-compose
sudo usermod -aG docker $USER
id $USER # make sure user is in docker group
```

Log out, log back in

```shell
docker run --rm hello-world
```

## Install Screenshots

![install options](images/rpi-install-00.png){width=50%}

![install config](images/rpi-install-01.png){width=50%}
