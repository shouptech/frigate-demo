# Ubiquiti UVC G3 with Frigate

1. (Optional) Factory reset camera. Press and hold the Reset button for more than 10 seconds while the camera is powered on.
2. Look up device's IP address in router's DHCP leases.
3. Navigate to `http://<ipaddress>` in web browser. Log in with `ubnt`/`ubnt`
4. At Setup Camera screen, click "Continue"
5. Click "Accept" on the EULA
6. Set Timezone to the current time zone. Change Mode to Standalone. Click "Continue".
7. At the device's config page, double check the mode is set to Standalaone. Switch to Standalone mode if not, and reboot the camera.
8. Set desired settings, such as display timestamp. I had better luck turning FPS to 15 and Bitrate to 3000. Also, set RTSP authentication and click Save Changes.
